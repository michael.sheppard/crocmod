/*
 * CrocodilesMod.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.crocs.common;

import com.crocs.client.*;
import net.minecraft.entity.*;
import net.minecraft.item.Food;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.SwampBiome;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;

import java.util.*;

import net.minecraftforge.common.BiomeDictionary.Type;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistryEntry;
import net.minecraftforge.registries.ObjectHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@SuppressWarnings("unused")
@Mod(CrocodilesMod.MODID)
public class CrocodilesMod {

    public static final String MODID = "crocmod";
    public static final String CROCMOD_NAME = "crocs";
    public static final String CROC_NAME = "croc";
    public static final String GATOR_NAME = "gator";
    public static final String LARGE_CROC_NAME = "large_croc";
    public static final String CAIMAN_NAME = "caiman";
    public static final String GAVIAL_NAME = "gavial";

    private static final Logger LOGGER = LogManager.getLogger(CrocodilesMod.MODID);

    // List of always excluded biome types
    private static final List<Type> excludedBiomeTypes = new ArrayList<>(Arrays.asList(
            Type.END,
            Type.NETHER,
            Type.VOID,
            Type.COLD,
            Type.OCEAN,
            Type.CONIFEROUS,
            Type.MOUNTAIN,
            Type.MUSHROOM,
            Type.SNOWY,
            Type.DRY
    ));

    public static final String COOKED_CROCMEAT = "cooked_crocmeat";
    public static final String CROCMEAT = "crocmeat";
    public static final String CROC_HIDE_NAME = "croc_hide";
    public static final String CROC_GROWL_NAME = "croc.growl";
    public static final String CROC_HISS_NAME = "croc.hiss";

    public CrocodilesMod() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::commonSetup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientSetup);

        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, ConfigHandler.commonSpec);

        MinecraftForge.EVENT_BUS.register(this);
        getLogger().debug("CrocodileMod Constructor finished");
    }

    public void commonSetup(final FMLCommonSetupEvent event) {
        ConfigHandler.loadConfig();

        registerSpawns();
        MinecraftForge.EVENT_BUS.register(new SpawnCheck());
        getLogger().debug("FMLCommonSetupEvent finished");
    }

    public void clientSetup(final FMLClientSetupEvent event) {
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.CROC, CrocRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.LARGE_CROC, LargeCrocRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.GATOR, GatorRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.CAIMAN, CaimanRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.GAVIAL, GavialRenderer::new);

        getLogger().debug("FMLClientSetupEvent finished");
    }

    @Mod.EventBusSubscriber(modid = CrocodilesMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
    @ObjectHolder(CrocodilesMod.MODID)
    public static class RegistryEvents {
        public static final EntityType<CrocEntity> CROC = EntityType.Builder.<CrocEntity>create(CrocEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(64).build(CrocodilesMod.MODID);

        public static final EntityType<GatorEntity> GATOR = EntityType.Builder.<GatorEntity>create(GatorEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(64).build(CrocodilesMod.MODID);

        public static final EntityType<LargeCrocEntity> LARGE_CROC = EntityType.Builder.<LargeCrocEntity>create(LargeCrocEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(64).build(CrocodilesMod.MODID);

        public static final EntityType<CaimanEntity> CAIMAN = EntityType.Builder.<CaimanEntity>create(CaimanEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(64).build(CrocodilesMod.MODID);

        public static final EntityType<GavialEntity> GAVIAL = EntityType.Builder.<GavialEntity>create(GavialEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(64).build(CrocodilesMod.MODID);

        public static final SoundEvent CROC_GROWL = new SoundEvent(new ResourceLocation(CrocodilesMod.MODID, CrocodilesMod.CROC_GROWL_NAME));
        public static final SoundEvent CROC_HURT = new SoundEvent(new ResourceLocation(CrocodilesMod.MODID, CrocodilesMod.CROC_HISS_NAME));

        @SubscribeEvent
        public static void onEntityRegistry(final RegistryEvent.Register<EntityType<?>> event) {
            event.getRegistry().registerAll(
                    setup(RegistryEvents.CROC, CrocodilesMod.CROC_NAME),
                    setup(RegistryEvents.GATOR, CrocodilesMod.GATOR_NAME),
                    setup(RegistryEvents.LARGE_CROC, CrocodilesMod.LARGE_CROC_NAME),
                    setup(RegistryEvents.CAIMAN, CrocodilesMod.CAIMAN_NAME),
                    setup(RegistryEvents.GAVIAL, CrocodilesMod.GAVIAL_NAME)
            );
            getLogger().debug("onEntityRegistry finished");
        }

        @SubscribeEvent
        public static void onItemRegistry(final RegistryEvent.Register<Item> event) {
            event.getRegistry().registerAll(
                    setup(new CrocMeatItem(new Item.Properties().group(ItemGroup.FOOD)
                            .food((new Food.Builder()).hunger(8).saturation(0.8f).meat().build())), COOKED_CROCMEAT),
                    setup(new CrocMeatItem(new Item.Properties().group(ItemGroup.FOOD)
                            .food((new Food.Builder()).hunger(3).saturation(0.3F).meat().build())), CROCMEAT),
                    setup(new Item(new Item.Properties().group(ItemGroup.MATERIALS)), CROC_HIDE_NAME),

                    setup(new SpawnEggItem(CROC, 0xCAFF70, 0x556B2F, new Item.Properties().group(ItemGroup.MISC)), "croc_spawn_egg"),
                    setup(new SpawnEggItem(GATOR, 0x2F4F4F, 0x8FBC8F, new Item.Properties().group(ItemGroup.MISC)), "gator_spawn_egg"),
                    setup(new SpawnEggItem(LARGE_CROC, 0x8B4513, 0x8B5A2B, new Item.Properties().group(ItemGroup.MISC)), "large_croc_spawn_egg"),
                    setup(new SpawnEggItem(CAIMAN, 0xDC143C, 0x8B6508, new Item.Properties().group(ItemGroup.MISC)), "caiman_spawn_egg"),
                    setup(new SpawnEggItem(GAVIAL, 0x3A5FCD, 0x8B6508, new Item.Properties().group(ItemGroup.MISC)), "gavial_spawn_egg")
            );
            getLogger().debug("onItemRegistry finished");
        }

        @SubscribeEvent
        public static void onSoundRegistry(final RegistryEvent.Register<SoundEvent> event) {
            CROC_GROWL.setRegistryName(CrocodilesMod.CROC_GROWL_NAME);
            CROC_HURT.setRegistryName(CrocodilesMod.CROC_HISS_NAME);
            getLogger().debug("onSoundRegistry finished");
        }

        public static <T extends IForgeRegistryEntry<T>> T setup(final T entry, final String name) {
            return setup(entry, new ResourceLocation(CrocodilesMod.MODID, name));
        }

        public static <T extends IForgeRegistryEntry<T>> T setup(final T entry, final ResourceLocation registryName) {
            entry.setRegistryName(registryName);
            return entry;
        }
    }

    private static void registerSpawns() {
        Biome[] biomes = getBiomes(Type.WET, Type.SWAMP, Type.RIVER, Type.BEACH, Type.WATER);

        int minSpawn = ConfigHandler.CommonConfig.getMinSpawn();
        int maxSpawn = ConfigHandler.CommonConfig.getMaxSpawn();

        registerEntitySpawn(RegistryEvents.CROC, biomes, ConfigHandler.CommonConfig.getCrocSpawnProb(), minSpawn, maxSpawn);
        registerEntitySpawn(RegistryEvents.GATOR, biomes, ConfigHandler.CommonConfig.getGatorSpawnProb(), minSpawn, maxSpawn);
        registerEntitySpawn(RegistryEvents.LARGE_CROC, biomes, ConfigHandler.CommonConfig.getLargeCrocSpawnProb(), minSpawn, maxSpawn);
        registerEntitySpawn(RegistryEvents.CAIMAN, biomes, ConfigHandler.CommonConfig.getCaimanSpawnProb(), minSpawn, maxSpawn);
        registerEntitySpawn(RegistryEvents.GAVIAL, biomes, ConfigHandler.CommonConfig.getGavialSpawnProb(), minSpawn, maxSpawn);

        getLogger().debug("registerSpawns finished");
    }

    private static Biome[] getBiomes(Type... types) {
        LinkedList<Biome> list = new LinkedList<>();
        Collection<Biome> biomes = ForgeRegistries.BIOMES.getValues();

        for (Biome biome : biomes) {
            Set<Type> bTypes = BiomeDictionary.getTypes(biome);

            // we exclude certain biomes, i.e., crocs are ectothermic, no cold biomes
            if (excludeThisBiome(bTypes)) {
                continue;
            }
            // process remaining biomes
            for (Type t : types) {
                if (BiomeDictionary.hasType(biome, t)) {
                    if (!list.contains(biome)) {
                        list.add(biome);
                        getLogger().info("Adding: " + biome.getRegistryName() + " biome for spawning");
                    }
                }
            }
        }
        return list.toArray(new Biome[0]);
    }

    private static boolean excludeThisBiome(Set<Type> types) {
        boolean excludeBiome = false;
        for (Type ex : excludedBiomeTypes) {
            if (types.contains(ex)) {
                excludeBiome = true;
                break;
            }
        }
        return excludeBiome;
    }

    private static void registerEntitySpawn(EntityType<? extends LivingEntity> type, Biome[] biomes, int spawnProb, int minSpawn, int maxSpawn) {
        if (spawnProb <= 0) {
            return; // do not spawn this entity
        }

        for (Biome bgb : biomes) {
            Biome biome = ForgeRegistries.BIOMES.getValue(bgb.getRegistryName());
            if (biome != null) {
                if (biome instanceof SwampBiome) {
                    spawnProb = spawnProb + 10;
                }
                biome.getSpawns(EntityClassification.CREATURE).add(new Biome.SpawnListEntry(type, spawnProb, minSpawn, maxSpawn));
            }
        }
        getLogger().debug("registerEntitySpawn finished (" + type.getTranslationKey() + ")");
    }

    public static Logger getLogger() {
        return LOGGER;
    }
}
