/*
 * ConfigHandler.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.crocs.common;

import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import net.minecraftforge.common.ForgeConfigSpec;
import org.apache.commons.lang3.tuple.Pair;

import java.nio.file.Paths;

public class ConfigHandler {

    public static void loadConfig() {
        CommentedFileConfig.builder(Paths.get("config", CrocodilesMod.CROCMOD_NAME, CrocodilesMod.MODID + ".toml")).build();
        CrocodilesMod.getLogger().debug("loadConfig finished");
    }

    public static class CommonConfig {
        public static ForgeConfigSpec.IntValue crocSpawnProb;
        public static ForgeConfigSpec.IntValue largeCrocSpawnProb;
        public static ForgeConfigSpec.IntValue gatorSpawnProb;
        public static ForgeConfigSpec.IntValue caimanSpawnProb;
        public static ForgeConfigSpec.IntValue gavialSpawnProb;
        public static ForgeConfigSpec.IntValue minSpawn;
        public static ForgeConfigSpec.IntValue maxSpawn;
        public static ForgeConfigSpec.BooleanValue randomScale;

        public CommonConfig(ForgeConfigSpec.Builder builder) {
            builder.comment("Crocodile Mod Config")
                   .push("CommonConfig");

            minSpawn = builder
                    .comment("Minimum number of crocs to spawn at one time")
                    .translation("config.crocs.minSpawn")
                    .defineInRange("minSpawn", 1, 1, 5);

            maxSpawn = builder
                    .comment("Maximum number of crocs to spawn at one time")
                    .translation("config.crocs.maxSpawn")
                    .defineInRange("maxSpawn", 4, 1, 8);

            crocSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.crocs.crocSpawnProb")
                    .defineInRange("crocSpawnProb", 10, 0, 100);

            caimanSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.crocs.caimanSpawnProb")
                    .defineInRange("caimanSpawnProb", 10, 0, 100);

            largeCrocSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.crocs.largeCrocSpawnProb")
                    .defineInRange("largeCrocSpawnProb", 10, 0, 100);

            gatorSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.crocs.gatorSpawnProb")
                    .defineInRange("gatorSpawnProb", 10, 0, 100);

            gavialSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.crocs.gavialSpawnProb")
                    .defineInRange("gavialSpawnProb", 10, 0, 100);

            randomScale = builder
                    .comment("Set to false to disable random scaling of monitors, default is true.")
                    .translation("config.crocs.randomScale")
                    .define("randomScale", true);

            builder.pop();
            CrocodilesMod.getLogger().debug("CommonConfig constructor finished");
        }

        public static boolean useRandomScaling() {
            return randomScale.get();
        }

        public static int getCrocSpawnProb() {
            return crocSpawnProb.get();
        }

        public static int getLargeCrocSpawnProb() {
            return largeCrocSpawnProb.get();
        }

        public static int getGatorSpawnProb() {
            return gatorSpawnProb.get();
        }

        public static int getCaimanSpawnProb() { return caimanSpawnProb.get(); }

        public static int getGavialSpawnProb() { return gavialSpawnProb.get(); }

        public static int getMinSpawn() {
            return minSpawn.get();
        }

        public static int getMaxSpawn() {
            return maxSpawn.get();
        }

    }

    static final ForgeConfigSpec commonSpec;
    public static final CommonConfig COMMON_CONFIG;
    static {
        final Pair<CommonConfig, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(CommonConfig::new);
        commonSpec = specPair.getRight();
        COMMON_CONFIG = specPair.getLeft();
    }
}
