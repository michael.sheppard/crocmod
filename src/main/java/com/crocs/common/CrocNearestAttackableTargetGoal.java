/*
 * CrocsNearestAttackableTargetGoal.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.crocs.common;

import net.minecraft.entity.EntityPredicate;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MobEntity;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.ai.goal.TargetGoal;
import net.minecraft.util.math.AxisAlignedBB;

import javax.annotation.Nullable;
import java.util.EnumSet;
import java.util.function.Predicate;

@SuppressWarnings("unused")
public class CrocNearestAttackableTargetGoal<T extends LivingEntity> extends TargetGoal {
    protected final Class<T> targetClass;
    protected final int targetChance;
    protected LivingEntity nearestTarget;
    protected EntityPredicate targetEntitySelector;

    public CrocNearestAttackableTargetGoal(MobEntity goalOwnerIn, Class<T> targetClassIn, boolean checkSight) {
        this(goalOwnerIn, targetClassIn, checkSight, false);
    }

    public CrocNearestAttackableTargetGoal(MobEntity goalOwnerIn, Class<T> targetClassIn, boolean checkSight, boolean nearbyOnlyIn) {
        this(goalOwnerIn, targetClassIn, 10, checkSight, nearbyOnlyIn, null);
    }

    public CrocNearestAttackableTargetGoal(MobEntity goalOwnerIn, Class<T> targetClassIn, int targetChanceIn, boolean checkSight, boolean nearbyOnlyIn, @Nullable Predicate<LivingEntity> targetPredicate) {
        super(goalOwnerIn, checkSight, nearbyOnlyIn);
        targetClass = targetClassIn;
        targetChance = targetChanceIn;
        setMutexFlags(EnumSet.of(Goal.Flag.TARGET));
        targetEntitySelector = (new EntityPredicate()).setDistance(getTargetDistance()).setCustomPredicate(targetPredicate);
    }

    public boolean shouldExecute() {
        if (nearestTarget != null && goalOwner.world.isDaytime()) {
            return goalOwner.getDistance(nearestTarget) < 1.0f;
        } else {
            if (goalOwner != null && targetChance > 0 && goalOwner.getRNG().nextInt(targetChance) != 0) {
                return false;
            } else {
                findNearestTarget();
                return nearestTarget != null;
            }
        }
    }

    protected AxisAlignedBB getTargetableArea(double targetDistance) {
        if (goalOwner.world.isDaytime()) {
            return goalOwner.getBoundingBox().grow(1.0, 1.0, 1.0);
        } else {
            return goalOwner.getBoundingBox().grow(targetDistance, 4.0D, targetDistance);
        }
    }

    protected void findNearestTarget() {
        nearestTarget = goalOwner.world.getClosestEntityWithinAABB(targetClass, targetEntitySelector, goalOwner,
                goalOwner.getPosX(), goalOwner.getPosYEye(), goalOwner.getPosZ(), getTargetableArea(getTargetDistance()));
    }

    public void startExecuting() {
        goalOwner.setAttackTarget(nearestTarget);
        super.startExecuting();
    }
}
