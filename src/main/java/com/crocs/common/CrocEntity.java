/*
 * CrocEntity.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.crocs.common;

import net.minecraft.block.BlockState;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.passive.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class CrocEntity extends CreatureEntity {

    private final float WIDTH = 1.0f;
    private final float HEIGHT = 0.3f;
    private final float scaleFactor;
    private static final DataParameter<Float> health = EntityDataManager.createKey(CrocEntity.class, DataSerializers.FLOAT);
    public EntitySize crocSize = new EntitySize(1.0f, 0.3f, false);

    public CrocEntity(EntityType<? extends CrocEntity> entity, World world) {
        super(entity, world);
        setHealth(10);

        if (ConfigHandler.CommonConfig.useRandomScaling()) {
            float scale = rand.nextFloat();
            scaleFactor = scale < 0.6F ? 1.0F : scale;
        } else {
            scaleFactor = 1.0F;
        }
        crocSize.scale(WIDTH * getScaleFactor(), HEIGHT * getScaleFactor());
    }

    @SuppressWarnings("unused")
    public CrocEntity(World world) {
        this(CrocodilesMod.RegistryEvents.CROC, world);
    }

    @Override
    protected void registerGoals() {
        goalSelector.addGoal(0, new SwimGoal(this));
        goalSelector.addGoal(1, new LeapAtTargetGoal(this, 0.5F));
        goalSelector.addGoal(2, new MeleeAttackGoal(this, 1.0, true));
        goalSelector.addGoal(3, new RandomWalkingGoal(this, 1.0));
        goalSelector.addGoal(4, new LookAtGoal(this, PlayerEntity.class, 8.0F));
        goalSelector.addGoal(5, new LookRandomlyGoal(this));

        targetSelector.addGoal(1, new HurtByTargetGoal(this));
        targetSelector.addGoal(2, new CrocNearestAttackableTargetGoal<>(this, PlayerEntity.class, true, true));
        targetSelector.addGoal(3, new CrocNearestAttackableTargetGoal<>(this, SheepEntity.class, false, true));
        targetSelector.addGoal(3, new CrocNearestAttackableTargetGoal<>(this, PigEntity.class, false, true));
        targetSelector.addGoal(3, new CrocNearestAttackableTargetGoal<>(this, RabbitEntity.class, false, true));
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    @Nonnull
    @Override
    public EntityType<?> getType() {
        return CrocodilesMod.RegistryEvents.CROC;
    }

    @Nonnull
    @Override
    public EntitySize getSize(@Nonnull Pose p) {
        return new EntitySize(WIDTH, HEIGHT, false);
    }

    @Override
    public void tick() {
        // kill in cold biomes
        Vec3d v = getPositionVec();
        BlockPos bp = new BlockPos(v.x, v.y, v.z);
        Biome biome = world.getBiome(bp);
        if (biome.getTemperature(bp) <= 0.25) {
            attackEntityFrom(DamageSource.STARVE, 4.0f);
        }
		super.tick();
	}

    @Override
    public boolean canDespawn(double distanceToPlayer) {
        return false;
    }

    @Override
    public void setAttackTarget(@Nullable LivingEntity livingEntity) {
        super.setAttackTarget(livingEntity);
    }

    @Override
    protected void registerAttributes() {
        super.registerAttributes();
        getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(10.0D);
        getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.25);
        getAttributes().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(3.0D);
    }

    @Override
    protected void registerData() {
        super.registerData();
        dataManager.register(health, getHealth());
    }

    @Nonnull
    @Override
    public ResourceLocation getLootTable() {
        return new ResourceLocation(CrocodilesMod.MODID, CrocodilesMod.CROC_NAME);
    }

    @Override
    protected SoundEvent getAmbientSound() {
        return CrocodilesMod.RegistryEvents.CROC_GROWL;
    }

    @Override
    protected SoundEvent getDeathSound() {
        return CrocodilesMod.RegistryEvents.CROC_GROWL;
    }

    @Override
    protected SoundEvent getHurtSound(@Nonnull DamageSource damageSourceIn) { return CrocodilesMod.RegistryEvents.CROC_HURT; }

    @Override
    protected void playStepSound(@Nonnull BlockPos pos, @Nonnull BlockState blockIn) {
        playSound(getStepSound(), 0.15F, 1.0F);
    }

    protected SoundEvent getStepSound() {
        return SoundEvents.ENTITY_COW_STEP;
    }

    @Override
    protected float getSoundVolume() {
        return 0.4F;
    }

    @Override
    protected int getExperiencePoints(@Nonnull PlayerEntity playerEntity) {
        return 1 + world.rand.nextInt(4);
    }

    @Override
    protected void updateAITasks() {
        dataManager.set(health, getHealth());
    }


    @Override
    public boolean attackEntityAsMob(@Nonnull Entity entity) {
        return entity.attackEntityFrom(DamageSource.causeMobDamage(this), 4);
    }

    // this makes the crocs move fast in water. Surprise!!
    @Override
    protected float getWaterSlowDown() {
        return 1.0F;
    }

    @Override
    public int getMaxSpawnedInChunk() {
        return 2;
    }
}
