/*
 * GavialRenderer.java
 *
 *  Copyright (c) 2019 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.crocs.client;

import com.crocs.common.CrocodilesMod;
import com.crocs.common.GavialEntity;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nonnull;

@OnlyIn(Dist.CLIENT)
public class GavialRenderer<T extends GavialEntity> extends MobRenderer<T, GavialModel<T>> {

    private static final ResourceLocation SKIN = new ResourceLocation(CrocodilesMod.MODID, "textures/entity/crocs/gavial.png");

    public GavialRenderer(EntityRendererManager rm) {
        super(rm, new GavialModel<>(), 0.0f);
        addLayer(new GavialEyeLayer<>(this));
    }

    @Override
    protected void preRenderCallback(T gavialEntity, MatrixStack matrixStack, float unknown) {
        float scaleFactor = gavialEntity.getScaleFactor();
        matrixStack.scale(scaleFactor, scaleFactor, scaleFactor);
    }

    @Override
    @Nonnull
    public ResourceLocation getEntityTexture(@Nonnull T entity) {
        return SKIN;
    }
}
