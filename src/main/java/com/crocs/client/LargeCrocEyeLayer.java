/*
 * LargeCrocEyeLayer.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.crocs.client;

import com.crocs.common.CrocodilesMod;
import com.crocs.common.LargeCrocEntity;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.AbstractEyesLayer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nonnull;

@OnlyIn(Dist.CLIENT)
public class LargeCrocEyeLayer<T extends LargeCrocEntity, M extends LargeCrocModel<T>> extends AbstractEyesLayer<T, M> {

    private static final RenderType EYES = RenderType.getEyes(new ResourceLocation(CrocodilesMod.MODID, "textures/entity/crocs/croc_eyes.png"));

    public LargeCrocEyeLayer(IEntityRenderer<T, M> renderer) {
        super(renderer);
    }

    @Override
    @Nonnull
    public RenderType getRenderType() {
        return EYES;
    }
}
