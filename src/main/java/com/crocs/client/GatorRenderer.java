/*
 * GatorRenderer.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.crocs.client;

import com.crocs.common.CrocodilesMod;
import com.crocs.common.GatorEntity;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nonnull;

@OnlyIn(Dist.CLIENT)
public class GatorRenderer<T extends GatorEntity> extends MobRenderer<T, GatorModel<T>> {

    private static final ResourceLocation SKIN = new ResourceLocation(CrocodilesMod.MODID, "textures/entity/crocs/gator.png");

    public GatorRenderer(EntityRendererManager rm) {
        super(rm, new GatorModel<>(), 0.0f);
        addLayer(new GatorEyeLayer<>(this));
    }

    @Override
    protected void preRenderCallback(T gatorEntity, MatrixStack matrixStack, float unknown) {
        float scaleFactor = gatorEntity.getScaleFactor();
        matrixStack.scale(scaleFactor, scaleFactor, scaleFactor);
    }

    @Override
    @Nonnull
    public ResourceLocation getEntityTexture(@Nonnull T entity) {
        return SKIN;
    }
}
