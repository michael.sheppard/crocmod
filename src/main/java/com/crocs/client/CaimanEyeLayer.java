/*
 * CaimanEyeLayer.java
 *
 *  Copyright (c) 2019 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.crocs.client;

import com.crocs.common.CaimanEntity;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.IEntityRenderer;
import net.minecraft.client.renderer.entity.layers.AbstractEyesLayer;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nonnull;

public class CaimanEyeLayer<T extends CaimanEntity, M extends CaimanModel<T>> extends AbstractEyesLayer<T, M> {

    private static final RenderType EYES = RenderType.getEyes(new ResourceLocation("crocmod", "textures/entity/crocs/caiman_eyes.png"));

    public CaimanEyeLayer(IEntityRenderer<T, M> renderer) {
        super(renderer);
    }

    @Override
    @Nonnull
    public RenderType getRenderType() {
        return EYES;
    }

}
