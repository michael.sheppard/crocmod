/*
 * LargeCrocModel.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.crocs.client;

import com.crocs.common.LargeCrocEntity;
import com.google.common.collect.ImmutableList;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

import javax.annotation.Nonnull;

public class LargeCrocModel<T extends LargeCrocEntity> extends SegmentedModel<T> {

	public ModelRenderer crocBody;
	public ModelRenderer crocHead;
	public ModelRenderer crocLeg1;
	public ModelRenderer crocLeg2;
	public ModelRenderer crocLeg3;
	public ModelRenderer crocLeg4;
	public ModelRenderer crocTail1;
	public ModelRenderer crocTail2;
	public ModelRenderer rfin;
	public ModelRenderer lfin;
	public ModelRenderer cfin;

	public LargeCrocModel()
	{
		float yPos = 19F;

		crocBody = new ModelRenderer(this, 0, 0);
		crocBody.addBox(-5F, -2F, -11F, 10, 4, 22);
		crocBody.setRotationPoint(0F, yPos, 0F);

		rfin = new ModelRenderer(this, 0, 10);
		rfin.addBox(3F, -2F, -10F, 0, 2, 20);
		rfin.setRotationPoint(0F, yPos - 2, 0F);

		lfin = new ModelRenderer(this, 0, 10);
		lfin.addBox(-3F, -2F, -10F, 0, 2, 20);
		lfin.setRotationPoint(0F, yPos - 2, 0F);

		cfin = new ModelRenderer(this, 0, 10);
		cfin.addBox(0F, -2F, -10F, 0, 2, 20);
		cfin.setRotationPoint(0F, yPos - 2, 0F);

		crocHead = new ModelRenderer(this, 50, 0);
		crocHead.addBox(-4F, 2F, -14F, 8, 3, 14);
		crocHead.setRotationPoint(0F, yPos - 3, -11F);

		crocLeg1 = new ModelRenderer(this, 50, 0);
		crocLeg1.addBox(0F, 0F, -2F, 3, 6, 4);
		crocLeg1.setRotationPoint(5F, yPos + 1F, -8F);
		crocLeg1.rotateAngleZ = 5.497787143782138F;

		crocLeg2 = new ModelRenderer(this, 50, 0);
		crocLeg2.addBox(0F, 0F, -2F, 3, 6, 4);
		crocLeg2.setRotationPoint(5F, yPos + 1F, 8F);
		crocLeg2.rotateAngleZ = 5.497787143782138F;

		crocLeg3 = new ModelRenderer(this, 50, 0);
		crocLeg3.mirror = true;
		crocLeg3.addBox(-3F, 0F, -2F, 3, 6, 4);
		crocLeg3.setRotationPoint(-5F, yPos + 1F, -8F);
		crocLeg3.rotateAngleZ = 0.7853981633974483F;

		crocLeg4 = new ModelRenderer(this, 50, 0);
		crocLeg4.mirror = true;
		crocLeg4.addBox(-3F, 0F, -2F, 3, 6, 4);
		crocLeg4.setRotationPoint(-5F, yPos + 1F, 8F);
		crocLeg4.rotateAngleZ = 0.7853981633974483F;

		crocTail1 = new ModelRenderer(this, 16, 15);
		crocTail1.addBox(-4F, -1F, 0F, 8, 3, 8);
		crocTail1.setRotationPoint(0F, yPos + 0F, 11F);

		crocTail2 = new ModelRenderer(this, 16, 14);
		crocTail2.addBox(-3F, -1F, -5F, 6, 2, 10);
		crocTail2.setRotationPoint(0F, yPos + 1F, 24F);

	}

	@Override
	@Nonnull
	public Iterable<ModelRenderer> getParts() {
		return ImmutableList.of(crocHead, crocBody, crocLeg1, crocLeg2, crocLeg3, crocLeg4, crocTail1, crocTail2, cfin, rfin, lfin);
	}

	@Override
	public void setRotationAngles(@Nonnull T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
		crocHead.rotateAngleX = headPitch / 57.29578F;
		crocHead.rotateAngleY = headPitch / 57.29578F;

		crocLeg1.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
		crocLeg2.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
		crocLeg3.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
		crocLeg4.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;

		crocTail1.rotateAngleY = MathHelper.cos(limbSwing * 0.6662F) * 0.4F * limbSwingAmount;
		crocTail2.rotateAngleY = MathHelper.sin(limbSwing * 0.6662F) * 0.4F * limbSwingAmount;
	}

}
